FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine AS build
WORKDIR /src
COPY WebService/* ./
RUN dotnet restore

FROM build AS publish
WORKDIR /src
RUN dotnet publish -c Release -o /src/publish

FROM mcr.microsoft.com/dotnet/aspnet:5.0-alpine AS runtime
ENV ASPNETCORE_URLS=http://*:80

WORKDIR /app
COPY --from=publish /src/publish .
CMD dotnet WebService.dll