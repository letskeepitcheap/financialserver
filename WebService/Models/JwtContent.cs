namespace WebService
{
    public class JwtContent
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}