using MediatR;

namespace WebService
{
    public class AddRecurringTransactionCommand : IRequest
    {
        public string Jwt { get; set; }
        public string? IncomeGeneratorId { get; set; }
        public RecurringTransactionRequest Request { get; set; }
    }
}