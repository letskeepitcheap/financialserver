using MediatR;

namespace WebService
{
    public class AddLedgerEntryCommand : IRequest
    {
        public string Jwt { get; set; }
        public LedgerEntryRequest Request { get; set; }
    }
}