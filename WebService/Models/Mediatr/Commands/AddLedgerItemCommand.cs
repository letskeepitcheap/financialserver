using MediatR;

namespace WebService
{
    public class AddLedgerItemCommand : IRequest
    {
        public string Jwt { get; set; }
        public AbstractLedgerItem LedgerItem { get; set; }
    }
}