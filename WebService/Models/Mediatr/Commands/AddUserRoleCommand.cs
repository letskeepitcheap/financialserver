using MediatR;

namespace WebService
{
    public class AddUserRoleCommand : IRequest
    {
        public string Jwt { get; set; }
        public string Description { get; set; }
    }
}