using MediatR;

namespace WebService
{
    public class DeleteIncomeGeneratorCommand : IRequest
    {
        public string Jwt { get; set; }
        public string Id { get; set; }
    }
}