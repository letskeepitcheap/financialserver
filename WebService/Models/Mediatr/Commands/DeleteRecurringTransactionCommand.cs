using MediatR;

namespace WebService
{
    public class DeleteRecurringTransactionCommand : IRequest
    {
        public string Jwt { get; set; }
        public string Id { get; set; }
    }
}