using MediatR;

namespace WebService
{
    public class DeleteLedgerEntryCommand : IRequest
    {
        public string Jwt { get; set; }
        public string Id { get; set; }
    }
}