using MediatR;

namespace WebService
{
    public class ResolveSupportTicketCommand : IRequest
    {
        public string Id { get; set; }
        public string Jwt { get; set; }
    }
}