using MediatR;

namespace WebService
{
    public class AddIncomeGeneratorQuery : IRequest<string>
    {
        public string Jwt { get; set; }
        public IncomeGeneratorRequest Request { get; set; }
    }
}