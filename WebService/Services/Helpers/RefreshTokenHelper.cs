using System;
using System.Security.Cryptography;

namespace WebService
{
    public class RefreshTokenHelper
    {
        public static string CreateRefresh(int size = 32)
        {
            var bytes = new byte[size];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            return Convert.ToBase64String(bytes);
        }
    }
}