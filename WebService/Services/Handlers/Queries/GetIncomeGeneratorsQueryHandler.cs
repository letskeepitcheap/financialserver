using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class GetIncomeGeneratorsQueryHandler : IRequestHandler<GetIncomeGeneratorsQuery, IEnumerable<IncomeGeneratorResponse>>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public GetIncomeGeneratorsQueryHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<IEnumerable<IncomeGeneratorResponse>> Handle(GetIncomeGeneratorsQuery query, CancellationToken cancellation)
        {
            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(query.Jwt);

            var transactionTypes = await _repo.GetAllAsync<TransactionType>();
            var generators = await _repo.GetIncomeGeneratorsByUserIdAsync(jwtContent.UserId);

            var responses = new List<IncomeGeneratorResponse>();
            foreach (var generator in generators)
            {
                var recurringTransactions = await _repo.GetRecurringTransactionsByIncomeGeneratorIdAsync(generator.Id);
                responses.Add(new IncomeGeneratorResponse()
                {
                    Id = generator.Id,
                    Description = generator.Description,
                    FrequencyId = generator.FrequencyId,
                    RecurringTransactions = from transaction in recurringTransactions
                                            select RecurringTransactionResponse.FromDBObject(transaction, transactionTypes)
                });
            }
            return responses;
        }
    }
}