using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class RefreshTokenQueryHandler : IRequestHandler<RefreshTokenQuery, Token>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private IUserRepository _repo;

        public RefreshTokenQueryHandler(ILogger logger, IJwtHandler jwtHandler, IUserRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Token> Handle(RefreshTokenQuery query, CancellationToken cancellation)
        {
            if (string.IsNullOrEmpty(query.Token.Jwt) || string.IsNullOrEmpty(query.Token.Refresh))
            {
                _logger.Throw($"Invalid token with jwt {query.Token.Jwt} and refresh {query.Token.Refresh}.");
            }

            JwtContent jwtContent = null;
            try
            {
                jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(query.Token.Jwt);
            }
            catch (ArgumentException ex)
            {
                _logger.Throw(ex.Message);
            }

            var refreshData = from data in await _repo.GetRefreshDataByUserIdAsync(jwtContent.UserId)
                              where data.Refresh == query.Token.Refresh
                              select data;

            if (!refreshData.Any())
            {
                _logger.Throw($"Unable to find refresh data with refresh {query.Token.Refresh}.");
            }

            var newToken = new Token()
            {
                Jwt = _jwtHandler.ConvertToJwt(jwtContent),
                Refresh = RefreshTokenHelper.CreateRefresh()
            };
            await _repo.DeleteRefreshDataByIdAsync(refreshData.First().Id);
            await _repo.InsertRefreshDataAsync(new RefreshData()
            {
                Refresh = newToken.Refresh,
                UserId = jwtContent.UserId,
                CreatedDate = DateTime.Now
            });
            return newToken;
        }
    }
}