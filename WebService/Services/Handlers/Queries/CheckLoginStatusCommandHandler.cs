using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class CheckLoginStatusQueryHandler : IRequestHandler<CheckLoginStatusQuery, LoginResponse>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private IUserRepository _repo;

        public CheckLoginStatusQueryHandler(ILogger logger, IJwtHandler jwtHandler, IUserRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<LoginResponse> Handle(CheckLoginStatusQuery query, CancellationToken cancellation)
        {
            JwtContent jwtContent = null;
            try
            {
                jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(query.Token.Jwt);
            }
            catch (ArgumentException ex)
            {
                _logger.Throw(ex.Message);
            }

            var users = await _repo.GetUsersByIdAsync(jwtContent.UserId);
            if (!users.Any())
            {
                _logger.Throw($"Unable to find user with id {jwtContent.UserId}.");
            }
            var user = users.First();
            return new LoginResponse()
            {
                Username = user.Username,
                Role = user.Role
            };
        }
    }
}