using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class AddIncomeGeneratorQueryHandler : IRequestHandler<AddIncomeGeneratorQuery, string>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public AddIncomeGeneratorQueryHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<string> Handle(AddIncomeGeneratorQuery query, CancellationToken cancellation)
        {
            _logger.Information($"Adding income generator with description {query.Request.Description}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(query.Jwt);

            var generator = await _repo.InsertIncomeGeneratorAsync(new IncomeGenerator()
            {
                UserId = jwtContent.UserId,
                Description = query.Request.Description,
                FrequencyId = query.Request.FrequencyId,
                CreatedDate = DateTime.Now
            });

            return generator.Id;
        }
    }
}