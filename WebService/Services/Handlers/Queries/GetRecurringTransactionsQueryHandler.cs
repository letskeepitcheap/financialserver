using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class GetRecurringTransactionsQueryHandler : IRequestHandler<GetRecurringTransactionsQuery, IEnumerable<RecurringTransactionResponse>>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public GetRecurringTransactionsQueryHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<IEnumerable<RecurringTransactionResponse>> Handle(GetRecurringTransactionsQuery query, CancellationToken cancellation)
        {
            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(query.Jwt);

            var transactionTypes = await _repo.GetAllAsync<TransactionType>();
            return from transaction in await _repo.GetRecurringTransactionsByUserIdAsync(jwtContent.UserId)
                   select RecurringTransactionResponse.FromDBObject(transaction, transactionTypes);
        }
    }
}