using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class AddRecurringTransactionCommandHandler : IRequestHandler<AddRecurringTransactionCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public AddRecurringTransactionCommandHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(AddRecurringTransactionCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Adding recurring transaction with description {command.Request.Description}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);
            var recurringTransaction = await _repo.InsertRecurringTransactionAsync(new RecurringTransaction()
            {
                UserId = jwtContent.UserId,
                Category = command.Request.Category,
                Description = command.Request.Description,
                Amount = new Decimal(command.Request.Amount),
                FrequencyId = command.Request.FrequencyId,
                IncomeGeneratorId = command.IncomeGeneratorId,
                TransactionTypeId = command.Request.TransactionTypeId,
                LastTriggered = command.Request.LastTriggered,
                CreatedDate = DateTime.Now
            });
            await _repo.InsertOrUpdateCategoryAsync(command.Request.Category);
            await _repo.InsertLedgerEntryAsync(new LedgerEntry()
            {
                UserId = jwtContent.UserId,
                Category = recurringTransaction.Category,
                Description = recurringTransaction.Description,
                Amount = recurringTransaction.Amount,
                TransactionTypeId = recurringTransaction.TransactionTypeId,
                RecurringTransactionId = recurringTransaction.Id,
                TransactionDate = recurringTransaction.LastTriggered,
                CreatedDate = DateTime.Now
            });
            return Unit.Value;
        }
    }
}