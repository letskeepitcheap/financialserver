using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class AddUserRoleCommandHandler : IRequestHandler<AddUserRoleCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private IUserRepository _repo;

        public AddUserRoleCommandHandler(ILogger logger, IJwtHandler jwtHandler, IUserRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(AddUserRoleCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Adding user role with description {command.Description}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);

            await _repo.InsertUserRoleAsync(new UserRole()
            {
                Description = command.Description,
                CreatedBy = jwtContent.UserId,
                CreatedDate = DateTime.Now
            });
            return Unit.Value;
        }
    }
}