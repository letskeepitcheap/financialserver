using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class AddMessageCommandHandler : IRequestHandler<AddMessageCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private IUserRepository _repo;
        public AddMessageCommandHandler(ILogger logger, IJwtHandler jwtHandler, IUserRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(AddMessageCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Adding message with subject {command.Request.Subject} to ticket {command.Request.TicketId}.");
            JwtContent jwtContent = null;
            try
            {
                jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Token.Jwt);
            }
            catch (ArgumentException ex)
            {
                _logger.Throw(ex.Message);
            }

            var tickets = await _repo.GetSupportTicketsByIdAsync(command.Request.TicketId);
            if (!tickets.Any())
            {
                _logger.Throw($"Unable to find support ticket with Id {command.Request.TicketId}.");
            }
            var ticket = tickets.First();

            // Only admins and the user who originally created the ticket are allowed to add
            // a message to the ticket.
            if (!jwtContent.Role.Equals("admin", StringComparison.InvariantCultureIgnoreCase) && ticket.SubmittedById != jwtContent.UserId)
            {
                _logger.Throw($"User with id {jwtContent.UserId} attempted to add a message to ticket {command.Request.TicketId}, but is not allowed.");
            }

            var users = await _repo.GetUsersByIdAsync(jwtContent.UserId);
            if (!users.Any())
            {
                _logger.Throw($"Unable to find user with id {jwtContent.UserId}.");
            }
            var user = users.First();

            await _repo.AddMessageToSupportTicketAsync(command.Request.TicketId, new Message()
            {
                SentBy = user.Username,
                Subject = command.Request.Subject,
                Content = command.Request.Content,
                Opened = false,
                CreatedDate = DateTime.Now
            });
            return Unit.Value;
        }
    }
}