using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class DeleteIncomeGeneratorCommandHandler : IRequestHandler<DeleteIncomeGeneratorCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public DeleteIncomeGeneratorCommandHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(DeleteIncomeGeneratorCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Deleting ledger entry with id {command.Id}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);

            var generators = await _repo.GetIncomeGeneratorsByIdAsync(command.Id);
            if (!generators.Any())
            {
                _logger.Throw($"Unable to find income generator with id {command.Id}.");
            }

            var generator = generators.First();
            if (generator.UserId != jwtContent.UserId)
            {
                _logger.Throw($"Income generator with id {command.Id} does not belong to user with id {jwtContent.UserId}.");
            }

            await _repo.DeleteIncomeGeneratorAsync(command.Id, jwtContent.UserId);
            await _repo.DeleteRecurringTransactionsByIncomeGeneratorIdAsync(command.Id, jwtContent.UserId);

            return Unit.Value;
        }
    }
}