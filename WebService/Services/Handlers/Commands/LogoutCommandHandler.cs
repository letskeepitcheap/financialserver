using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class LogoutCommandHandler : IRequestHandler<LogoutCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private IUserRepository _repo;

        public LogoutCommandHandler(ILogger logger, IJwtHandler jwtHandler, IUserRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(LogoutCommand command, CancellationToken cancellation)
        {
            JwtContent jwtContent = null;
            try
            {
                jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Token.Jwt);
            }
            catch (ArgumentException ex)
            {
                _logger.Throw(ex.Message);
            }

            var ids = from data in await _repo.GetRefreshDataByUserIdAsync(jwtContent.UserId)
                      select data.Id;
            foreach (var id in ids)
            {
                // In case the user has more than one refresh data stored.
                await _repo.DeleteRefreshDataByIdAsync(id);
            }
            return Unit.Value;
        }
    }
}