using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class AddLedgerItemCommandHandler : IRequestHandler<AddLedgerItemCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public AddLedgerItemCommandHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(AddLedgerItemCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Adding ledger item with description {command.LedgerItem.Description}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);
            command.LedgerItem.CreatedBy = jwtContent.UserId;
            command.LedgerItem.CreatedDate = DateTime.Now;

            await _repo.InsertOneAsync(command.LedgerItem);
            return Unit.Value;
        }
    }
}