using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class AddLedgerEntryCommandHandler : IRequestHandler<AddLedgerEntryCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public AddLedgerEntryCommandHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(AddLedgerEntryCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Adding ledger entry with description {command.Request.Description}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);
            await _repo.InsertOrUpdateCategoryAsync(command.Request.Category);
            var entry = await _repo.InsertLedgerEntryAsync(new LedgerEntry()
            {
                UserId = jwtContent.UserId,
                Category = command.Request.Category,
                Description = command.Request.Description,
                Amount = new Decimal(command.Request.Amount),
                TransactionTypeId = command.Request.TransactionTypeId,
                TransactionDate = command.Request.TransactionDate,
                RecurringTransactionId = command.Request.RecurringTransactionId,
                CreatedDate = DateTime.Now
            });

            return Unit.Value;
        }
    }
}