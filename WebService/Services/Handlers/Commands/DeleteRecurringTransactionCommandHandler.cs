using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class DeleteRecurringTransactionCommandHandler : IRequestHandler<DeleteRecurringTransactionCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public DeleteRecurringTransactionCommandHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(DeleteRecurringTransactionCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Deleting recurring transaction with id {command.Id}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);

            await _repo.DeleteRecurringTransactionAsync(command.Id, jwtContent.UserId);
            return Unit.Value;
        }
    }
}