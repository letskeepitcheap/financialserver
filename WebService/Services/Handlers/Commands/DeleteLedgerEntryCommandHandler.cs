using System.Threading;
using System.Threading.Tasks;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class DeleteLedgerEntryCommandHandler : IRequestHandler<DeleteLedgerEntryCommand>
    {
        private ILogger _logger;
        private IJwtHandler _jwtHandler;
        private ILedgerRepository _repo;

        public DeleteLedgerEntryCommandHandler(ILogger logger, IJwtHandler jwtHandler, ILedgerRepository repo)
        {
            _logger = logger;
            _jwtHandler = jwtHandler;
            _repo = repo;
        }

        public async Task<Unit> Handle(DeleteLedgerEntryCommand command, CancellationToken cancellation)
        {
            _logger.Information($"Deleting ledger entry with id {command.Id}.");

            var jwtContent = _jwtHandler.ExtractFromJwt<JwtContent>(command.Jwt);
            await _repo.DeleteLedgerEntryAsync(command.Id, jwtContent.UserId);
            return Unit.Value;
        }
    }
}