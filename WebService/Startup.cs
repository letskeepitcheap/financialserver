using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                var origins = Configuration["ORIGINS"].Split(',');
                options.AddPolicy("allow",
                builder => builder.WithOrigins(origins)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                );
            });

            services.AddMemoryCache();
            services.AddMvc().AddFluentValidation();
            services.AddMediatR(typeof(Startup));
            services.AddScoped<IRequestHandler<GetLedgerItemsQuery<Frequency>, IEnumerable<Frequency>>, GetLedgerItemsQueryHandler<Frequency>>();
            services.AddScoped<IRequestHandler<GetLedgerItemsQuery<TransactionType>, IEnumerable<TransactionType>>, GetLedgerItemsQueryHandler<TransactionType>>();

            var logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.File("./log.log")
                .WriteTo.Console()
                .CreateLogger();
            services.AddScoped<ILogger>(s => logger);

            services.AddScoped<ILedgerRepository>(s =>
                new LedgerRepository(s.GetRequiredService<IMemoryCache>(), Configuration["MONGO_DB"], Configuration["LEDGER_DB"]));
            services.AddScoped<IUserRepository>(s =>
                new UserRepository(Configuration["MONGO_DB"], Configuration["USERS_DB"]));

            services.AddTransient<IValidator<CreateUserRequest>, CreateUserRequestValidator>();
            services.AddTransient<IValidator<IncomeGeneratorRequest>, IncomeGeneratorRequestValidator>();
            services.AddTransient<IValidator<LedgerEntryRequest>, LedgerEntryRequestValidator>();
            services.AddTransient<IValidator<LoginRequest>, LoginRequestValidator>();
            services.AddTransient<IValidator<RecurringTransactionRequest>, RecurringTransactionRequestValidator>();
            services.AddTransient<IValidator<FrequencyRequest>, FrequencyRequestValidator>();
            services.AddTransient<IValidator<TransactionTypeRequest>, TransactionTypeRequestValidator>();
            services.AddTransient<IValidator<UserRoleRequest>, UserRoleRequestValidator>();

            services.AddRSJwt(JwtConfig.BaseConfig(Configuration["JWT_SECRET"], Configuration["ISSUER"], Configuration["AUDIENCE"]));
            services.AddControllers();
            // services.AddSwaggerGen(c =>
            // {
            //     c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebService", Version = "v1" });
            // });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // app.UseDeveloperExceptionPage();
            // app.UseSwagger();
            // app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebService v1"));

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors("allow");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
