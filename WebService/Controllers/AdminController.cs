using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService.Controllers
{
    [ApiController]
    [Route("api/admin")]
    [RSJwtAuthorization("Role", "Admin")]
    public class AdminController : ControllerBase
    {
        private readonly ILogger _logger;
        private IMediator _mediatr;

        public AdminController(ILogger logger, IMediator mediatr)
        {
            _logger = logger;
            _mediatr = mediatr;
        }

        [HttpGet]
        [Route("")]
        // Like UserController.CheckLoggedIn, if the request gets this far, it's good.
        public ActionResult CheckAdmin() => new OkResult();

        [HttpPost]
        [Route("frequency")]
        public async Task<ActionResult> AddFrequency([FromBody] FrequencyRequest request) =>
            await AddLedgerItem(new Frequency()
            {
                Description = request.Description,
                ApproxTimesPerYear = request.ApproxTimesPerYear
            });

        [HttpPost]
        [Route("transactiontype")]
        public async Task<ActionResult> AddTransactionType([FromBody] TransactionTypeRequest request) =>
            await AddLedgerItem(new TransactionType()
            {
                Description = request.Description
            });

        private async Task<ActionResult> AddLedgerItem(AbstractLedgerItem item)
        {
            try
            {
                await _mediatr.Send(new AddLedgerItemCommand()
                {
                    Jwt = GetJwtFromCookie(),
                    LedgerItem = item
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpPost]
        [Route("role")]
        public async Task<ActionResult> CreateUserRole([FromBody] UserRoleRequest request)
        {
            try
            {
                await _mediatr.Send(new AddUserRoleCommand()
                {
                    Description = request.Description,
                    Jwt = GetJwtFromCookie()
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        // [HttpPost]
        // [Route("user/role")]
        // public async Task<ActionResult> ChangeUserRole([FromBody] UpdateUserRoleRequest request)
        // {
        //     var userId = GetUserIdFromCookie();
        //     if (string.IsNullOrEmpty(userId))
        //     {
        //         return new UnauthorizedResult();
        //     }
        //     await _service.UpdateUserRoleAsync(request, userId);
        //     return new OkResult();
        // }

        [HttpPatch]
        [Route("ticket/{id}/resolve")]
        public async Task<ActionResult> ResolveTicket(string id)
        {
            try
            {
                await _mediatr.Send(new ResolveSupportTicketCommand()
                {
                    Id = id,
                    Jwt = GetJwtFromCookie()
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
        }

        private string GetJwtFromCookie()
        {
            if (!HttpContext.Request.Cookies.TryGetValue("jwt", out var jwt))
            {
                return "";
            }
            return jwt;
        }
    }
}