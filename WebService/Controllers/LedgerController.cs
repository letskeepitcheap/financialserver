using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using MediatR;
using LD.RSJwt;

namespace WebService.Controllers
{
    [ApiController]
    [Route("api/ledger")]
    [RSJwtAuthorization("Role", new[] { "Admin", "User" })]
    public class LedgerController : ControllerBase
    {
        private readonly ILogger _logger;
        private IMediator _mediatr;

        public LedgerController(ILogger logger, IMediator mediatr)
        {
            _logger = logger;
            _mediatr = mediatr;
        }

        [HttpGet]
        [Route("{start}/{end}")]
        public async Task<ActionResult<IEnumerable<LedgerEntryResponse>>> GetLegerEntries(string start, string end)
        {
            try
            {
                var entries = await _mediatr.Send(new GetLedgerEntriesQuery()
                {
                    Jwt = GetJwtFromCookie(),
                    Start = start,
                    End = end
                });
                return new OkObjectResult(entries);
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
        }

        [HttpPost]
        [Route("")]
        public async Task<ActionResult> InsertLedgerEntry([FromBody] LedgerEntryRequest request)
        {
            try
            {
                await _mediatr.Send(new AddLedgerEntryCommand()
                {
                    Jwt = GetJwtFromCookie(),
                    Request = request
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> DeleteLedgerEntry(string id)
        {
            try
            {
                await _mediatr.Send(new DeleteLedgerEntryCommand()
                {
                    Jwt = GetJwtFromCookie(),
                    Id = id
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpGet]
        [Route("generators")]
        public async Task<ActionResult<IEnumerable<IncomeGeneratorResponse>>> GetIncomeGenerators()
        {
            try
            {
                var generators = await _mediatr.Send(new GetIncomeGeneratorsQuery()
                {
                    Jwt = GetJwtFromCookie()
                });
                return new OkObjectResult(generators);
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
        }

        [HttpPost]
        [Route("generator")]
        public async Task<ActionResult> AddIncomeGenerator([FromBody] IncomeGeneratorRequest request)
        {
            try
            {
                var jwt = GetJwtFromCookie();
                var generatorId = await _mediatr.Send(new AddIncomeGeneratorQuery()
                {
                    Jwt = jwt,
                    Request = request,
                });

                foreach (var transaction in request.RecurringTransactions)
                {
                    await _mediatr.Send(new AddRecurringTransactionCommand()
                    {
                        Jwt = jwt,
                        IncomeGeneratorId = generatorId,
                        Request = transaction
                    });
                }
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpDelete]
        [Route("generator/{id}")]
        public async Task<ActionResult> DeleteIncomeGenerator(string id)
        {
            try
            {
                await _mediatr.Send(new DeleteIncomeGeneratorCommand()
                {
                    Jwt = GetJwtFromCookie(),
                    Id = id
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
        }

        [HttpGet]
        [Route("recurringtransactions")]
        public async Task<ActionResult<IEnumerable<RecurringTransactionResponse>>> GetRecurringTransactions()
        {
            try
            {
                var transactions = await _mediatr.Send(new GetRecurringTransactionsQuery()
                {
                    Jwt = GetJwtFromCookie()
                });
                return new OkObjectResult(transactions);
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpPost]
        [Route("recurringtransaction")]
        public async Task<ActionResult> AddRecurringTransaction([FromBody] RecurringTransactionRequest request)
        {
            try
            {
                await _mediatr.Send(new AddRecurringTransactionCommand()
                {
                    Jwt = GetJwtFromCookie(),
                    Request = request
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpDelete]
        [Route("recurringtransaction/{id}")]
        public async Task<ActionResult> DeleteRecurringTransaction(string id)
        {
            try
            {
                await _mediatr.Send(new DeleteRecurringTransactionCommand()
                {
                    Jwt = GetJwtFromCookie(),
                    Id = id
                });
                return new OkResult();
            }
            catch (ArgumentNullException)
            {
                return new UnauthorizedResult();
            }
        }

        [HttpGet]
        [Route("categories/{partial}")]
        public async Task<ActionResult<IEnumerable<string>>> GetLedgerEntryCategories(string partial) =>
            new OkObjectResult(await _mediatr.Send(new GetCategoriesQuery() { Partial = partial }));

        [HttpGet]
        [Route("frequencies")]
        public async Task<ActionResult<IEnumerable<Frequency>>> GetFrequencies() => new OkObjectResult(await _mediatr.Send(new GetLedgerItemsQuery<Frequency>()));

        [HttpGet]
        [Route("transactiontypes")]
        public async Task<ActionResult<IEnumerable<TransactionType>>> GetTransactionTypes() => new OkObjectResult(await _mediatr.Send(new GetLedgerItemsQuery<TransactionType>()));

        private string GetJwtFromCookie()
        {
            if (!HttpContext.Request.Cookies.TryGetValue("jwt", out var jwt))
            {
                return "";
            }
            return jwt;
        }
    }
}
