using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class GetLedgerEntriesQueryHandlerShould
    {
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private GetLedgerEntriesQueryHandler _handler;

        public GetLedgerEntriesQueryHandlerShould()
        {
            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>())).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new GetLedgerEntriesQueryHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task ThrowIfInvalidStartDate()
        {
            var query = CreateValidQuery();
            query.Start = Guid.NewGuid().ToString();
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfInvalidEndDate()
        {
            var query = CreateValidQuery();
            query.End = Guid.NewGuid().ToString();
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ReturnEntries()
        {
            var query = CreateValidQuery();
            var entries = await _handler.Handle(query, new CancellationToken());
            Assert.NotNull(entries);
        }

        private GetLedgerEntriesQuery CreateValidQuery() =>
            new GetLedgerEntriesQuery()
            {
                Jwt = Guid.NewGuid().ToString(),
                Start = NowMilliseconds().ToString(),
                End = NowMilliseconds().ToString()
            };

        private double NowMilliseconds()
        {
            DateTime start = new DateTime(1970, 1, 1);
            var span = DateTime.Now - start;
            return span.TotalMilliseconds;
        }
    }
}