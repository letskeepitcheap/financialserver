using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using BC = BCrypt.Net.BCrypt;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class LoginQueryHandlerShould
    {
        private string _username = Guid.NewGuid().ToString();
        private string _password = Guid.NewGuid().ToString();
        private Mock<IUserRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private LoginQueryHandler _handler;

        public LoginQueryHandlerShould()
        {
            IEnumerable<User> users = new List<User>()
            {
                new User()
                {
                    Username = _username,
                    PasswordHash = BC.HashPassword(_password)
                }
            };
            var logger = new Mock<ILogger>();
            _repo = new Mock<IUserRepository>();

            _jwtHandler = new Mock<IJwtHandler>();
            _repo.Setup(x => x.GetUsersByUsernameAsync(It.IsAny<string>()))
                .Returns<string>(username => Task.FromResult(username == _username ? users : new List<User>()));

            _handler = new LoginQueryHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task ThrowIfInvalidUsername()
        {
            var query = CreateValidQuery();
            query.Request.Username = Guid.NewGuid().ToString();
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfIncorrectPassword()
        {
            var query = CreateValidQuery();
            query.Request.Password = Guid.NewGuid().ToString();
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ReturnLoginResponse()
        {
            var query = CreateValidQuery();
            var response = await _handler.Handle(query, new CancellationToken());
            Assert.NotNull(response);
            Assert.Equal(_username, response.Username);
        }

        private LoginQuery CreateValidQuery() =>
            new LoginQuery()
            {
                Request = new LoginRequest()
                {
                    Username = _username,
                    Password = _password
                }
            };
    }
}