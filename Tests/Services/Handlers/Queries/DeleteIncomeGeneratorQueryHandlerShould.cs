using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class DeleteIncomeGeneratorQueryHandlerShould
    {
        private string _validJwt = Guid.NewGuid().ToString();
        private string _invalidJwt = Guid.NewGuid().ToString();
        private string _validUserId = Guid.NewGuid().ToString();
        private string _invalidUserId = Guid.NewGuid().ToString();
        private string _validId = Guid.NewGuid().ToString();
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private DeleteIncomeGeneratorCommandHandler _handler;

        public DeleteIncomeGeneratorQueryHandlerShould()
        {
            IEnumerable<IncomeGenerator> generators = new List<IncomeGenerator>() { new IncomeGenerator() { UserId = _validUserId } };
            IEnumerable<IncomeGenerator> noGenerators = new List<IncomeGenerator>();


            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();
            _repo.Setup(x => x.GetIncomeGeneratorsByIdAsync(It.IsAny<string>()))
                .Returns<string>(id => Task.FromResult(id == _validId ? generators : noGenerators));

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_validJwt)).Returns(new JwtContent() { UserId = _validUserId });
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_invalidJwt)).Returns(new JwtContent() { UserId = _invalidUserId });

            _handler = new DeleteIncomeGeneratorCommandHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task ThrowIfInvalidId()
        {
            var query = CreateValidCommand();
            query.Id = Guid.NewGuid().ToString();
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfWrongUser()
        {
            var query = CreateValidCommand();
            query.Jwt = _invalidJwt;
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task DeleteIncomeGenerator()
        {
            var query = CreateValidCommand();
            await _handler.Handle(query, new CancellationToken());
            _repo.Verify(x => x.DeleteIncomeGeneratorAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _repo.Verify(x => x.DeleteRecurringTransactionsByIncomeGeneratorIdAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        private DeleteIncomeGeneratorCommand CreateValidCommand() =>
            new DeleteIncomeGeneratorCommand()
            {
                Id = _validId,
                Jwt = _validJwt
            };
    }
}