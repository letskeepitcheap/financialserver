using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class RefreshTokenQueryHandlerShould
    {
        private Token _token;
        private string _validUserId = Guid.NewGuid().ToString();
        private string _validJwt = Guid.NewGuid().ToString();
        private string _invalidJwt = Guid.NewGuid().ToString();
        private Mock<IUserRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private RefreshTokenQueryHandler _handler;

        public RefreshTokenQueryHandlerShould()
        {
            _token = new Token()
            {
                Jwt = _validJwt,
                Refresh = Guid.NewGuid().ToString()
            };
            IEnumerable<RefreshData> data = new List<RefreshData>()
            {
                new RefreshData()
                {
                    Refresh = _token.Refresh
                }
            };

            var logger = new Mock<ILogger>();
            _repo = new Mock<IUserRepository>();
            _repo.Setup(x => x.GetRefreshDataByUserIdAsync(It.IsAny<string>()))
                .Returns<string>(id => Task.FromResult(id == _validUserId ? data : new List<RefreshData>()));

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_validJwt)).Returns(new JwtContent() { UserId = _validUserId });
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_invalidJwt)).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new RefreshTokenQueryHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task ThrowIfEmptyJwt()
        {
            var query = CreateValidQuery();
            query.Token.Jwt = "";
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfEmptyRefresh()
        {
            var query = CreateValidQuery();
            query.Token.Refresh = "";
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfBadJwt()
        {
            var query = CreateValidQuery();
            query.Token.Jwt = _invalidJwt;
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfNoRefreshData()
        {
            var query = CreateValidQuery();
            query.Token = new Token() { Jwt = Guid.NewGuid().ToString() };
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ReturnRefreshData()
        {
            var query = CreateValidQuery();
            var token = await _handler.Handle(query, new CancellationToken());
            Assert.NotNull(token);
        }

        private RefreshTokenQuery CreateValidQuery() => new RefreshTokenQuery() { Token = _token };
    }
}