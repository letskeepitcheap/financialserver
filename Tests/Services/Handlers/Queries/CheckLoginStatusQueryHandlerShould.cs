using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class CheckLoginStatusQueryHandlerShould
    {
        private string _validJwt = Guid.NewGuid().ToString();
        private string _invalidJwt = Guid.NewGuid().ToString();
        private string _validUserId = Guid.NewGuid().ToString();
        private string _invalidUserId = Guid.NewGuid().ToString();
        private Mock<IUserRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private CheckLoginStatusQueryHandler _handler;

        public CheckLoginStatusQueryHandlerShould()
        {
            var logger = new Mock<ILogger>();
            _repo = new Mock<IUserRepository>();
            _repo.SetupUserRepo(_invalidUserId);

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_validJwt)).Returns(new JwtContent() { UserId = _validUserId });
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_invalidJwt)).Returns(new JwtContent() { UserId = _invalidUserId });

            _handler = new CheckLoginStatusQueryHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task ThrowIfBadJwt()
        {
            var query = CreateValidQuery();
            query.Token.Jwt = _invalidJwt;
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ThrowIfNoUsers()
        {
            var query = new CheckLoginStatusQuery()
            {
                Token = new Token()
                {
                    Jwt = _invalidJwt
                }
            };

            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(query, new CancellationToken()));
        }

        [Fact]
        public async Task ReturnLoginResponse()
        {
            var query = CreateValidQuery();
            var response = await _handler.Handle(query, new CancellationToken());
            Assert.NotNull(response);
        }

        private CheckLoginStatusQuery CreateValidQuery() => new CheckLoginStatusQuery() { Token = new Token() { Jwt = _validJwt } };
    }
}