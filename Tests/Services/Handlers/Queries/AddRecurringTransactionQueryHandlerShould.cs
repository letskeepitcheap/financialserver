using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class AddRecurringTransactionCommandHandlerShould
    {
        private string _id = Guid.NewGuid().ToString();
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private AddRecurringTransactionCommandHandler _handler;

        public AddRecurringTransactionCommandHandlerShould()
        {
            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();
            _repo.Setup(x => x.InsertRecurringTransactionAsync(It.IsAny<RecurringTransaction>()))
                .Returns<RecurringTransaction>(x =>
                {
                    x.Id = _id;
                    return Task.FromResult(x);
                });

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>())).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new AddRecurringTransactionCommandHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task InsertRecurringTransaction()
        {
            var query = CreateValidCommand();

            await _handler.Handle(query, new CancellationToken());

            _repo.Verify(x => x.InsertRecurringTransactionAsync(It.IsAny<RecurringTransaction>()), Times.Once);
            _repo.Verify(x => x.InsertOrUpdateCategoryAsync(It.IsAny<string>()), Times.Once);
            _repo.Verify(x => x.InsertLedgerEntryAsync(It.IsAny<LedgerEntry>()), Times.Once);
        }

        private AddRecurringTransactionCommand CreateValidCommand() =>
            new AddRecurringTransactionCommand()
            {
                Request = new RecurringTransactionRequest()
            };
    }
}