using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class DeleteRecurringTransactionCommandHandlerShould
    {
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private DeleteRecurringTransactionCommandHandler _handler;

        public DeleteRecurringTransactionCommandHandlerShould()
        {
            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>())).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new DeleteRecurringTransactionCommandHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task DeleteRecurringTransaction()
        {
            var command = new DeleteRecurringTransactionCommand()
            {
                Id = Guid.NewGuid().ToString(),
                Jwt = Guid.NewGuid().ToString()
            };
            await _handler.Handle(command, new CancellationToken());
            _repo.Verify(x => x.DeleteRecurringTransactionAsync(command.Id, It.IsAny<string>()), Times.Once);
        }
    }
}