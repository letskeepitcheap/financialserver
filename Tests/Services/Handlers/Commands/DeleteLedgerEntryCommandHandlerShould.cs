using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class DeleteLedgerEntryCommandHandlerShould
    {
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private DeleteLedgerEntryCommandHandler _handler;

        public DeleteLedgerEntryCommandHandlerShould()
        {
            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>())).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new DeleteLedgerEntryCommandHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task DeleteLedgerEntry()
        {
            var command = new DeleteLedgerEntryCommand()
            {
                Id = Guid.NewGuid().ToString(),
                Jwt = Guid.NewGuid().ToString()
            };

            await _handler.Handle(command, new CancellationToken());
            _repo.Verify(x => x.DeleteLedgerEntryAsync(command.Id, It.IsAny<string>()), Times.Once);
        }
    }
}