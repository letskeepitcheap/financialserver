using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class AddIncomeGeneratorCommandHandlerShould
    {
        private string _validId = Guid.NewGuid().ToString();
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private AddIncomeGeneratorQueryHandler _handler;

        public AddIncomeGeneratorCommandHandlerShould()
        {
            var generator = new IncomeGenerator() { Id = _validId };

            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();
            _repo.Setup(x => x.InsertIncomeGeneratorAsync(It.IsAny<IncomeGenerator>()))
                .Returns(Task.FromResult(generator));

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>())).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new AddIncomeGeneratorQueryHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task InsertIncomeGenerator()
        {
            var command = new AddIncomeGeneratorQuery()
            {
                Jwt = Guid.NewGuid().ToString(),
                Request = new IncomeGeneratorRequest()
            };

            var id = await _handler.Handle(command, new CancellationToken());
            Assert.Equal(_validId, id);
            _repo.Verify(x => x.InsertIncomeGeneratorAsync(It.IsAny<IncomeGenerator>()), Times.Once);
            _jwtHandler.Verify(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>()), Times.Once);
        }
    }
}