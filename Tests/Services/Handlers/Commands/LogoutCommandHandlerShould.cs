using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class LogoutCommandHandlerShould
    {
        private string _validJwt = Guid.NewGuid().ToString();
        private string _invalidJwt = Guid.NewGuid().ToString();
        private Mock<IUserRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private LogoutCommandHandler _handler;

        public LogoutCommandHandlerShould()
        {
            IEnumerable<RefreshData> data = new List<RefreshData>()
            {
                new RefreshData(),
                new RefreshData()
            };
            var logger = new Mock<ILogger>();
            _repo = new Mock<IUserRepository>();
            _repo.Setup(x => x.GetRefreshDataByUserIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(data));

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_invalidJwt)).Throws(new ArgumentException());
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(_validJwt)).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new LogoutCommandHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task ThrowIfBadJwt()
        {
            var command = CreateValidCommand();
            command.Token.Jwt = _invalidJwt;
            await Assert.ThrowsAsync<ArgumentException>(async () => await _handler.Handle(command, new CancellationToken()));
        }

        [Fact]
        public async Task DeleteAllRefreshData()
        {
            var command = CreateValidCommand();
            await _handler.Handle(command, new CancellationToken());
            _repo.Verify(x => x.GetRefreshDataByUserIdAsync(It.IsAny<string>()), Times.Once);
            _repo.Verify(x => x.DeleteRefreshDataByIdAsync(It.IsAny<String>()), Times.Exactly(2));
        }

        private LogoutCommand CreateValidCommand() => new LogoutCommand() { Token = new Token() { Jwt = _validJwt } };
    }
}