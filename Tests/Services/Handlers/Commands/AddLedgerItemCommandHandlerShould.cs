using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Serilog;
using WebService;
using LD.RSJwt;

namespace Tests
{
    public class AddLedgerItemCommandHandlerShould
    {
        private Mock<ILedgerRepository> _repo;
        private Mock<IJwtHandler> _jwtHandler;
        private AddLedgerItemCommandHandler _handler;

        public AddLedgerItemCommandHandlerShould()
        {
            var logger = new Mock<ILogger>();
            _repo = new Mock<ILedgerRepository>();

            _jwtHandler = new Mock<IJwtHandler>();
            _jwtHandler.Setup(x => x.ExtractFromJwt<JwtContent>(It.IsAny<string>())).Returns(new JwtContent() { UserId = Guid.NewGuid().ToString() });

            _handler = new AddLedgerItemCommandHandler(logger.Object, _jwtHandler.Object, _repo.Object);
        }

        [Fact]
        public async Task InsertIncomeGenerator()
        {
            var command = new AddLedgerItemCommand()
            {
                Jwt = Guid.NewGuid().ToString(),
                LedgerItem = new TransactionType()
                {
                    Description = Guid.NewGuid().ToString()
                }
            };

            await _handler.Handle(command, new CancellationToken());

            _repo.Verify(x => x.InsertOneAsync<AbstractLedgerItem>(It.IsAny<TransactionType>()), Times.Once);
        }
    }
}